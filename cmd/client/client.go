package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi16/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi16Client struct {
	pb.Gogi16Client
}

func NewGogi16Client(address string) *Gogi16Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi16 service", l.Error(err))
	}

	c := pb.NewGogi16Client(conn)

	return &Gogi16Client{c}
}
